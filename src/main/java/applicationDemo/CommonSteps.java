package applicationDemo;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;

public class CommonSteps {
	
	
	
	
	
	
	
	public static void EditLead() throws InterruptedException {
		// TODO Auto-generated method stub
		
		String comp2 = "Cognizant";
		
		
		 System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		 driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click(); 
		
		
		
		
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("mohammed");;
		driver.findElementByXPath("//button[text()='Find Leads']").sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		driver.findElementByLinkText("mohammed").click();
		
		//driver.findElementByXPath("//a[text()='mohammed']").click();
		
		String title = driver.getTitle();
		
		if (title.equals("View Lead | opentaps CRM"))
		{
			
			System.out.println("Title matches" + " " + "View Lead | opentaps CRM");
		}
		
		
		driver.findElementByXPath("//a[text()=\"Edit\"]").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys(comp2);
		driver.findElementByXPath("//input[@value='Update']").click();
		
		String text = driver.findElementById("viewLead_companyName_sp").getText();
		
		
		if (text.contains(comp2)) {
			
			System.out.println("Company name changed to" + " " + comp2);
			
		}
		
		driver.close();
		
				
	}

	
	
	
	
	
	public static void DeleteLead() throws InterruptedException {
		
		
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
	
		driver.findElementByXPath("//span[text()='Phone']").click();
		driver.findElementByXPath("//input[@name='phoneNumber']").sendKeys("9500801893");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		Thread.sleep(3000);
		String id = driver.findElementByXPath("(//a[@class='linktext'])[4]").getText();
		driver.findElementByLinkText("mohammed").click();
		driver.findElementByXPath("//a[text()='Delete']").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("//input[@name='id']").sendKeys(id);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		Thread.sleep(3000);
		String errorMsg = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
		
		if (errorMsg.equals("No records to display")) {
			
			
			System.out.println("Lead with id" + " " + id + " " + "deleted successfully");
			
		}
		
		
		driver.close();
		
		
	}
	
	
	
	public static void DuplicateLead() throws InterruptedException {
	
	
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		
		
		
		driver.findElementByXPath("//span[text()='Email']").click();
		driver.findElementByXPath("//input[@name='emailAddress']").sendKeys("abc@gmail");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		String leadName = driver.findElementByXPath("(//a[@class='linktext'])[6]").getText();
		
		driver.findElementByXPath("(//a[@class='linktext'])[6]").click();
		driver.findElementByXPath("//a[text()='Duplicate Lead']").click();
		
		String duplicate = driver.findElementByXPath("//div[text()='Duplicate Lead']").getText();
		
		if (duplicate.equals("Duplicate Lead")) {
			
			System.out.println("Title Matches");
		}
		
		
		driver.findElementByXPath("//input[@value='Create Lead']").click();
		
		Thread.sleep(3000);
		
		String viewName = driver.findElementById("viewLead_firstName_sp").getText();
		
		if (viewName.equals(leadName)) {
			
			System.out.println(viewName + " " + "matches with" + " " + leadName);
		}else
			System.out.println("no match");
		
		driver.close();
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
