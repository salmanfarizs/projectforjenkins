package applicationDemo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Window.Type;
import javax.swing.UIManager;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AutomationApp {

	private JFrame frmAutomationapp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		CommonSteps obj1 = new CommonSteps();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AutomationApp window = new AutomationApp();
					window.frmAutomationapp.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AutomationApp() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAutomationapp = new JFrame();
		frmAutomationapp.setBackground(new Color(0, 0, 0));
		frmAutomationapp.setForeground(new Color(51, 153, 255));
		frmAutomationapp.setTitle("AutomationApp");
		frmAutomationapp.setResizable(false);
		frmAutomationapp.setBounds(100, 100, 450, 142);
		frmAutomationapp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAutomationapp.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("EditLead");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CommonSteps obj1 = new CommonSteps();
				try {
					obj1.EditLead();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnNewButton.setForeground(new Color(51, 51, 0));
		btnNewButton.setBackground(new Color(255, 255, 204));
		btnNewButton.setBounds(58, 53, 89, 23);
		frmAutomationapp.getContentPane().add(btnNewButton);
		
		JButton btnDeletelead = new JButton("DeleteLead");
		btnDeletelead.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CommonSteps obj1 = new CommonSteps();
				try {
					obj1.DeleteLead();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnDeletelead.setForeground(new Color(51, 51, 0));
		btnDeletelead.setBackground(new Color(255, 255, 204));
		btnDeletelead.setBounds(168, 53, 89, 23);
		frmAutomationapp.getContentPane().add(btnDeletelead);
		
		JButton btnDuplicateLead = new JButton("Duplicate Lead");
		btnDuplicateLead.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CommonSteps obj1 = new CommonSteps();
				try {
					obj1.DuplicateLead();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnDuplicateLead.setForeground(new Color(51, 51, 0));
		btnDuplicateLead.setBackground(new Color(255, 255, 204));
		btnDuplicateLead.setBounds(274, 53, 89, 23);
		frmAutomationapp.getContentPane().add(btnDuplicateLead);
		frmAutomationapp.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{frmAutomationapp.getContentPane()}));
	}
}
