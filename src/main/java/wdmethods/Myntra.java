package wdmethods;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Myntra extends SeMethods {
	
	@Test
	public void homwork() {
	 startApp("chrome","https://www.myntra.com/");
	 //type(locateElement("XPath", "//a[@id='header-search-icon']"),"Sunglasses");
	 type(locateElement("class", "desktop-searchBar"),"Sunglasses");
	 press("Enter");
	 
	 List<WebElement> list = driver.findElementsByXPath("//div[@class='product-brand']");
	
	 for (WebElement webElement : list) {
		System.out.println(webElement.getText());
		 
	}
	
	
	 
	 List<WebElement> list3 = driver.findElementsByXPath("//span[contains(text(),'40%')]/preceding::h4[contains(text(),'Unisex')][1]");
	 
	 
	 for (WebElement webElement : list3) {
		
		 String text = webElement.getText();
		
			 
			 System.out.println(text);
		}
	 
	 
	 List<WebElement> list4 = driver.findElementsByXPath("//span[contains(text(),'40%')]/preceding::"
	 		+ "h4[contains(text(),'Unisex')][1]/preceding::div[@class='product-brand'][1]");
	 
	 for (WebElement webElement : list4) {
		
		 System.out.println("Brand name is" + " " + webElement.getText());
	}
	 
	 List<WebElement> list5 = driver.findElementsByXPath("//span[contains(text(),'40%')]/preceding::h4["
	 		+ "contains(text(),'Unisex')][1]/following::span[@class='product-discountedPrice'][1]");
	 
	 
	 for (WebElement webElement : list5) {
		
		 System.out.println("Price is" + " " + webElement.getText());
	}
	 
	 
	 click(locateElement("XPath", "//h4[text()='Face Shape']"));
	 
	 
	 Actions builder = new Actions(driver);
	 
	 
	 /*WebDriverWait wait = new WebDriverWait(driver, 30);
	 
	 WebElement element = locateElement("XPath", "//label[@class='common-customCheckbox']//input[@value='oval sunglasses']");
	 wait.until(ExpectedConditions.visibilityOf(element));
	 click(element);
	 */
	 builder.click(locateElement("XPath", "//label[@class='common-customCheckbox']//input[@value='round']")).perform();
	 
	 click(locateElement("XPath", "//h4[text()='Type']"));
	 
	 builder.click(locateElement("XPath", "//label[@class='common-customCheckbox']//input[@value='oval sunglasses']")).perform();
	 
	 click(locateElement("XPath", "(//img[@class='img-responsive'])[1]"));
	
	 switchToWindow(1);
	 
	 System.out.println(locateElement("XPath", "//h1[@class='pdp-name']").getText());
	 
	 String text = locateElement("XPath", "//h1[@class='pdp-name']").getText();
	 
	 click(locateElement("XPath", "//div[text()='ADD TO BAG']"));
	 
	 click(locateElement("XPath", "//span[text()='GO TO BAG']"));
	 
	 String text1 = locateElement("XPath", "//div[@class='prod-name']").getText();
	
	 System.out.println(text);
	 System.out.println(text1);
	 
	 if (text1.contains(text)) {
		 
		 System.out.println( text + " " + "equals" + " " + text1);
	 }else
		 System.out.println("no match");
	 
	 
	 closeBrowser();
	 
	 
	 
	 
	
}}
