package wdmethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import week3.day1.Report;

public class SeMethods extends Report implements WdMethods {

	protected static RemoteWebDriver driver = null;
	@Override
	public void startApp(String browser, String url) {
	
		
	try {
		if (browser.equalsIgnoreCase("chrome")) {
			
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		}
		driver.get(url);
		driver.manage().window().maximize();
		reportStep("Pass", "The browser "+browser+" launched successfully");
	} catch (WebDriverException e) {
		// TODO Auto-generated catch block
		System.err.println("WebDriverException");
		reportStep("Fail", "Browser not launched");
		throw new RuntimeException();
	}
	}

	@Override
	public WebElement locateElement(String locator, String locValue) {
		//try {
			switch (locator) {
			case "id": return driver.findElementById(locValue);
				
			case "class": return  driver.findElementByClassName(locValue);
			case "LinkText": return driver.findElementByLinkText(locValue);
			
			case "XPath" : return driver.findElementByXPath(locValue);
			
			}
		/*} catch (NoSuchElementException e) {
			System.err.println("NosuchElement");
			e.printStackTrace();
			reportStep("Fail", "Element not found");
		}catch (WebDriverException e) {
			
			System.err.println("Webdriver exception");
			reportStep("Fail", "Element not found");
			
		}*/
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		
		
		
		return locateElement("id", locValue);
	}

	@Override
	public void type(WebElement ele, String data) {
		
		
       try {
		ele.sendKeys(data);	
		   reportStep("pass", "The data "+data+" entered successfully");
	} catch (WebDriverException e) {
		System.err.println("WebDriverException occured" + e.getMessage());
		
		reportStep("Fail", "The data "+data+" not entered");
		throw new RuntimeException();
	}
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			reportStep("pass", "The element "+ele+ "click successfully");
		} catch (WebDriverException e) {
			System.err.println("WebDriverException occured");
			reportStep("Fail", "The element "+ele+ "not clicked");
			throw new RuntimeException();
		
		}
	}

	@Override
	public String getText(WebElement ele) {
		// TODO Auto-generated method stub
		
		return ele.getText();
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
	try {
		Select sel = new Select(ele);
		sel.selectByVisibleText(value);
		reportStep("pass", "The text "+value+" entered successfully");
	} catch (WebDriverException e) {
		// TODO Auto-generated catch block
		System.err.println("Webdriver exception");
		reportStep("fail", "The text "+value+" not entered");
	}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		try {
			Select sel = new Select(ele);
			sel.selectByIndex(index);
			reportStep("pass", "The index "+index+" entered successfully");
		} catch (WebDriverException e) {
			System.err.println("Webdriver exception");
			reportStep("fail", "The index "+index+" not entered");
		
		}
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub
		try {
			Select sel = new Select(ele);
			sel.selectByValue(value);
		
			reportStep("pass", "The value "+value+" entered successfully");
		} catch (WebDriverException e) {
			System.err.println("Webdriver exception");
			reportStep("fail", "The value "+value+" not entered");
		
		}
	}
	
	
	
	
	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		
		 	
		try {
			if ((driver.getTitle()).equals(expectedTitle)) {
				
				return true;
			}
			
			
		} catch (WebDriverException e) {

			System.err.println("title not found" + e.getMessage());
			reportStep("fail", "title not found");
			
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		
		try {
			if((ele.getText().equals(expectedText))) {
				
				reportStep("pass", (ele +" "+ "macthes the text"));
			}
		} catch (WebDriverException e) {
			System.err.println(e.getMessage());
			reportStep("fail", ele + " " +  "doesnot match");
		}
		
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		
		try {
			if((ele.getText().contains(expectedText))) {
				
				reportStep("pass", (ele +" "+ "contains the text"));
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			reportStep("fail", ele + " " +  "doesnot contain");
		}
		
		
		
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		if (ele.getAttribute(attribute).equals(value)) {
			
			reportStep("pass", "attribute matches");
		}else
			reportStep("fail", "attribute matches");
		
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
		
		
if (ele.getAttribute(attribute).contains(value)) {
			
					reportStep("pass", "partial attribute matches");
		}else
			reportStep("fail", "No attribute matches");
	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		if(ele.isSelected()) {
			
			reportStep("pass", ele + " " + "is Selected");
		}else
			reportStep("fail", ele + " " + "is not Selected");
		
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		
		if(ele.isDisplayed() ) {
			
			reportStep("pass", ele + " " + "is Displayed");
			
		}else
			reportStep("fail", ele + " " + "is not Displayed");
		
	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		try {
			Set<String> windowHandles = driver.getWindowHandles();
			List<String> list = new ArrayList<String>();
			list.addAll(windowHandles);
			driver.switchTo().window(list.get(index));
			reportStep("pass", "Switched window succesfully");
		} catch (WebDriverException e) {
			
			System.err.println(e.getMessage());
			reportStep("fail", "Did not switch window");
		}
		
		
		
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		
		try {
			driver.switchTo().frame(ele);
			reportStep("pass", "switched frame successfully");
		} catch (WebDriverException e) {

			
			System.err.println(e.getMessage());
			reportStep("fail", "Did not switch frame");
		}
		
		
	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().accept();
			reportStep("pass", "accepted alert");
		} catch (WebDriverException e) {
			
			System.err.println(e.getMessage());
			reportStep("fail", "Did not find alert");
			
		}
		
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().dismiss();
			reportStep("pass", "dismissed alert");
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
			reportStep("fail", "Did not find alert");
		}
	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		return driver.switchTo().alert().getText();
	}

	@Override
	public void takeSnap() {
		// TODO Auto-generated method stub
		int i=0;
		File src = driver.getScreenshotAs(OutputType.FILE);
		 File dec = new File("./Snaps/img.jpeg" + i);
		 try {
			FileUtils.copyFile(src, dec);
			reportStep("pass", "Snap taken sucessfully");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Could not take snap due to I/O");
			reportStep("fail", "Snap not taken");
		} 
		 i++;
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		try {
			driver.close();
			reportStep("pass", "Browser closed");
		} catch (Exception e) {
			reportStep("fail", "Browser not closed");
			System.err.println(e.getMessage());
		}
	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		driver.quit();
	}

	@Override
	public void maximize() {
		// TODO Auto-generated method stub
	
		driver.manage().window().maximize();
		
	}

	@Override
	public Actions press(String value) {
		// TODO Auto-generated method stub
		Actions builder = new Actions(driver);
		
		switch (value.toUpperCase()) {
		case  "CONTROL":  builder.sendKeys(Keys.CONTROL).perform();
		break;
		case "ENTER":  builder.sendKeys(Keys.ENTER).perform();	
		break;	

		
		}
		return builder;
		
			
	}

	@Override
	public void explicitWait() {
		// TODO Auto-generated method stub
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		
		
	}

}
