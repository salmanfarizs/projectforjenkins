package wdmethods;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class SpecificMethod extends SeMethods {
	
	
	@BeforeSuite
	public void book() {
		
		System.out.println( "Lets Book ticket" );
		
	}
	
	
	@Parameters({"browser","url"})
	@BeforeMethod
	public void launch(String browser , String url) {
		
		startApp(browser, url);
		
		
		
	}
	
	
}
