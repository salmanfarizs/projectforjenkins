package wdmethods;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Zoom1Car extends SeMethods {
	
	@Test(invocationCount = 2)
	public void car() {
		
		startApp("chrome", "https://www.zoomcar.com/chennai/");
		click(locateElement("LinkText", "Start your wonderful journey"));
		click(locateElement("XPath", "(//div[@class='items'])[1]"));
		click(locateElement("XPath", "//button[text()='Next']"));
		
		
		Date date = new Date();
		DateFormat sdf = new SimpleDateFormat("dd");
		
		String today = sdf.format(date);
		
		int tomorrow = Integer.parseInt(today) + 1;
		
		System.out.println(tomorrow);
		
		
		
		//System.out.println(locateElement("XPath", "//div[contains(text(),'29')]").getText());
		
		click(locateElement("XPath",  "//div[contains(text(),'"+tomorrow+"')]"));		
		click(locateElement("XPath", "//button[text()='Next']"));
		
		if(locateElement("XPath", "(//div[contains(text(),'"+tomorrow+"')])[1]").isEnabled())
			
		{
			
			System.out.println("selected");
			
			
		}else System.out.println("not selected");
		
		click(locateElement("XPath", "//button[text()='Done']"));
		
		List<WebElement> list = driver.findElementsByXPath("(//div[@class='price'])");
		
		System.out.println(driver.findElementByXPath("(//div[@class='price'])").getText());
		System.out.println(list.size());
		
		List<String> list1 = new ArrayList<String>();
		
		for (WebElement webElement : list) {
			
			String a = webElement.getText();
			String b = a.replaceAll("\\D", "");
			
			list1.add(b);
			
			System.out.println(b);
			
			
		}
		
		System.out.println("max is " + Collections.max(list1));
		
		String max = Collections.max(list1);

		String text = locateElement("XPath", "//div[contains(text(),'"+max+"')]/preceding::h3[1]").getText();
		
		System.out.println(text);
		
		locateElement("XPath", "//div[contains(text(),'"+max+"')]/following::button").click();
	}

}
