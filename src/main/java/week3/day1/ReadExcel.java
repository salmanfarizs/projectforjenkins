package week3.day1;

import java.io.IOException;
import java.util.Date;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.omg.PortableServer.RequestProcessingPolicyOperations;

import wdmethods.SeMethods;

public class ReadExcel extends ProjectSpecificMethods {
	
	public static String[][] readData(String filename) throws IOException {
		
		XSSFWorkbook wbook = new XSSFWorkbook("./excel/"+filename+".xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		System.out.println(rowCount);
		
		int colCount = sheet.getRow(0).getLastCellNum();
		System.out.println(colCount);
		String[][] data = new String[rowCount][colCount];
		for (int i = 1; i <= rowCount; i++) {
			
			XSSFRow row = sheet.getRow(i);
			
			
			for (int j = 0; j < colCount; j++) {
				try {
					XSSFCell column = row.getCell(j);
					//CellType type = column.getCellTypeEnum();
					
					data[i-1][j] = column.getStringCellValue();
										}
				catch (NullPointerException e) {
					
					//reportStep("fail", "Data in Excel is Null");
					System.err.println(e.getMessage()+ " " + "value entered in excel");
					data[i-1][j] = "";
					
				}
				
				
			}
			
			
		}
		
		return data;
	}

	
	
public static String[][] readData1(String filename) throws IOException {
		
		XSSFWorkbook wbook = new XSSFWorkbook("./Myown/"+filename+".xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		System.out.println(rowCount);
		
		int colCount = sheet.getRow(0).getLastCellNum();
		System.out.println(colCount);
		
		String[][] data = new String[rowCount][colCount];
		for (int i = 1; i <= rowCount; i++) {
			
			XSSFRow row = sheet.getRow(i);
		
			for (int j = 0; j < colCount; j++) {
				XSSFCell column = row.getCell(j);
				data[i-1][j] = column.getStringCellValue();
			
			}
			
			
		}
		
		return data;
	}
}
