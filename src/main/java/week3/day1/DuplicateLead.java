package week3.day1;

import java.io.IOException;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DuplicateLead extends ProjectSpecificMethods{
	
	
	@BeforeTest
	public void setData() {
		testcaseName="TC01_Duplicatelead";
		testdesp="Duplicate Lead";
		author="Salman";
		category="Sanity";
	}
	
	@Test(dataProvider = "getforDup")
	public  void DuplicateLead(String email) throws InterruptedException {
		
		
		
		click(locateElement("LinkText","Leads" ));
		
		
		click(locateElement("XPath", "//a[text()='Find Leads']"));
		
		click(locateElement("XPath", "//span[text()='Email']"));
		type(locateElement("XPath", "//input[@name='emailAddress']"), email);
		
		click(locateElement("XPath", "//button[text()='Find Leads']"));
		
		Thread.sleep(3000);
		
		String leadName = locateElement("XPath", "(//a[@class='linktext'])[6]").getText();
		
		click(locateElement("XPath", "(//a[@class='linktext'])[6]"));
		click(locateElement("XPath", "//a[text()='Duplicate Lead']"));
		
		
		
		String duplicate = locateElement("XPath", "//div[text()='Duplicate Lead']").getText();
		
		if (duplicate.equals("Duplicate Lead")) {
			
			System.out.println("Title Matches");
		}
		
		click(locateElement("XPath", "//input[@value='Create Lead']"));
		
		Thread.sleep(3000);
		
		String viewName = locateElement("viewLead_firstName_sp").getText();
		
		if (viewName.equals(leadName)) {
			
			System.out.println(viewName + " " + "matches with" + " " + leadName);
		}else
			System.out.println("no match");
		
		
		
	}
	
	@DataProvider(name = "getforDup")
	public String[][] getData() throws IOException {
		
		
		return ReadExcel.readData("DupLead");
	}
	
	
	
	

}
