package week3.day1;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import wdmethods.SeMethods;

public class ProjectSpecificMethods extends SeMethods {
	
	
	
	@Parameters({"browser" , "url", "username" , "pwd"})
	@BeforeMethod (groups = {"common"})
	public void login(String browser,String url ,String username , String pwd) {
		
		startApp(browser, url);
		type(locateElement("username"), username);
		type(locateElement("password"), pwd);
		click(locateElement("class", "decorativeSubmit"));
		click(locateElement("LinkText", "CRM/SFA"));
		
		
		
	}
	
	
	
	
	
	@AfterMethod (groups = {"common"})
	public void close() {
		
		closeBrowser();
		
		
		
	}

}
