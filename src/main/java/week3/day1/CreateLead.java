package week3.day1;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.util.TypeKey;



public class CreateLead extends ProjectSpecificMethods {
	
	
	@BeforeTest
	public void setData() {
		testcaseName="TC01_CreateLead";
		testdesp="Create Lead";
		author="Salman";
		category="Smoke";
	}
	
	@Test(groups = {"smoke"} , dataProvider = "get")
	public void createLead(String x1, String x2, String x3, String ph) throws InterruptedException , NoSuchElementException {
	
	
	
	
	click(locateElement("LinkText", "Leads"));
	click(locateElement("LinkText", "Create Lead"));
	
	
	  
	type(locateElement("createLeadForm_companyName"), x1);
	type(locateElement("createLeadForm_firstName"), x2);
	type(locateElement("createLeadForm_lastName"), x3);
	
	type(locateElement("createLeadForm_primaryPhoneNumber"), ph);
	
	
	
	
	/*WebElement eleSource = driver.findElementById("createLeadForm_dataSourceId");
	WebElement eleCamp = driver.findElementById("createLeadForm_marketingCampaignId");
	WebElement eleInd = driver.findElementById("createLeadForm_industryEnumId");
	
	
	Select sc = new Select(eleSource);
	sc.selectByVisibleText("Website");
	
	Select camp = new Select(eleCamp);
	camp.selectByValue("CATRQ_AUTOMOBILE");
	
	Select ind = new Select(eleInd);
	
	//Java Collections
	List<WebElement> options = ind.getOptions();
	
	int count = options.size();
	
	System.out.println("For example");
	for (int i =0 ; i < count ; i++) {
		
		
		
		WebElement eachoption = options.get(i);
		System.out.println(eachoption.getText());
		
	}
	
	
	
	System.out.println("For each example");
	
	
	for(WebElement eachOption : options) {
		
		
		System.out.println(eachOption.getText());
		
	}
		
		
	
	driver.findElementById("createLeadForm_firstNameLocal").sendKeys("mohammed");
	driver.findElementById("createLeadForm_generalProfTitle").sendKeys("TestLead");
	driver.findElementById("createLeadForm_annualRevenue").sendKeys("6,00,000");
	//driver.findElementById("createLeadForm_industryEnumId").sendKeys("Computer Software");
	driver.findElementById("createLeadForm_ownershipEnumId").sendKeys("Corporation");
	driver.findElementById("createLeadForm_sicCode").sendKeys("625002");
	driver.findElementById("createLeadForm_lastNameLocal").sendKeys("fariz");
	driver.findElementById("createLeadForm_departmentName").sendKeys("Testing");
	driver.findElementById("createLeadForm_currencyUomId").sendKeys("INR - Indian Rupee");
	driver.findElementById("createLeadForm_numberEmployees").sendKeys("200");
	driver.findElementById("createLeadForm_tickerSymbol").sendKeys("yes");
	driver.findElementById("createLeadForm_description").sendKeys("this is the description");
	driver.findElementById("createLeadForm_importantNote").sendKeys("nothing");
	driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("001");
	driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("625");
	driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("0452");
	driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9500801893");
	
	driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("nobody");
	driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.google.com");
	driver.findElementById("createLeadForm_generalToName").sendKeys("salman");
	driver.findElementById("createLeadForm_generalAttnName").sendKeys("fariz");
	driver.findElementById("createLeadForm_generalAddress1").sendKeys("22");
	driver.findElementById("createLeadForm_generalAddress2").sendKeys("road");
	driver.findElementById("createLeadForm_generalCity").sendKeys("madurai");
	driver.findElementById("createLeadForm_generalCountryGeoId").sendKeys("India");
	driver.findElementByXPath("//*[@id=\"createLeadForm_generalStateProvinceGeoId\"]").sendKeys("TAMILNADU");
	driver.findElementById("createLeadForm_generalPostalCode").sendKeys("625002");
	driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("002");*/	
	type(locateElement("createLeadForm_primaryEmail"), "abc@gmail.com");
	click(locateElement("class", "smallSubmit"));
	Thread.sleep(2000);
	
	try {
		if(locateElement("XPath", "//div[text()='View Lead']").isDisplayed()) 
		{
			reportStep("pass", "Created Lead Sucessfully");
		}
	} catch (Exception e) {
		String errorText = locateElement("XPath", "//li[@class='errorMessage']").getText();
		if (errorText.contains("[crmsfa.createLead.companyName]")) {
			
			reportStep("Pass", "Negative testcase for companyname passed");
			}else if(errorText.contains("firstName")) {
				reportStep("pass","Negative testcase for firstname passed");
			}else if(errorText.contains("lastName")) {
				
				reportStep("pass","Negative testcase for lastname passed");
				
			}
	}
	
	
	
		
	
	
}
	
@DataProvider(name="get")
public String[][] getData() throws IOException {
	
	return ReadExcel.readData("CreateLead");
	
	
	/*String[][] data = new String[2][4];
	data[0][0] = "CGI";
	data[0][1] = "salman001";		
	data[0][2]= "fariz";
	data[0][3] = "9500801893";
	
	data[1][0] = "Wipro";
	data[1][1] = "salman";		
	data[1][2]= "fariz001";
	data[1][3] = "9944951646";
	return data;*/
	
	
	
	
}



















}
