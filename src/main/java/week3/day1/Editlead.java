package week3.day1;

import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import cucumber.api.java.it.Data;
import junit.framework.Assert;

public class Editlead extends ProjectSpecificMethods {
	
	@BeforeTest
	public void setData() {
		testcaseName="TC01_Editlead";
		testdesp="Edit Lead";
		author="Salman";
		category="Sanity";
	}
	
	
	
	@Test(/*groups = {"sanity"} , dependsOnGroups = {"smoke"}*/  alwaysRun = true , 
			dataProvider = "getforEdit")
	public void editL(String searchName, String newCompanyName) throws InterruptedException {
	
	String comp2 = "Cognizant";
	
	click(locateElement("LinkText","Leads" ));
	
	
	click(locateElement("XPath", "//a[text()='Find Leads']"));

	
	
	
	type(locateElement("XPath", "(//input[@name='firstName'])[3]"), searchName);
	click(locateElement("XPath", "//button[text()='Find Leads']"));
	Thread.sleep(3000);
	click(locateElement("LinkText",searchName ));
	
	//driver.findElementByXPath("//a[text()='mohammed']").click();
	
	/*String title = driver.getTitle();
	
	if (title.equals("View Lead | opentaps CRM"))
	{
		
		System.out.println("Title matches" + " " + "View Lead | opentaps CRM");
	}
	*/
	
	click(locateElement("XPath", "//a[text()='Edit']"));
	
	locateElement("updateLeadForm_companyName").clear();
	locateElement("updateLeadForm_companyName").sendKeys(newCompanyName);
	
	click(locateElement("XPath", "//input[@value='Update']"));
	
	
	String text = locateElement("viewLead_companyName_sp").getText();
	
	Assert.assertEquals(comp2, text);
	/*if (text.contains(comp2)) {
		
		System.out.println("Company name changed to" + " " + comp2);
		
	}*/}
	

@DataProvider(name="getforEdit")
public String[][] getData() throws IOException {
	
	return ReadExcel.readData("EditLead");
	
	
	
	
}













}
