package week3.day1;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import gherkin.ast.Location;


public class DeleteLead extends ProjectSpecificMethods {
	
	@BeforeTest
	public void setData() {
		testcaseName="TC01_Deletelead";
		testdesp="Delete Lead";
		author="Salman";
		category="Sanity";
	}
	
@Test(groups = {"reg"} , dataProvider = "getforDelete"  /*dependsOnGroups = {"smoke"}*/)
public  void Delete(String phoneNo) throws InterruptedException {
		
	 Scanner in = new Scanner(System.in);
		
		click(locateElement("LinkText", "Leads"));
		
		click(locateElement("XPath", "//a[text()='Find Leads']"));
		Thread.sleep(3000);
		
	//	type(locateElement("XPath", "(//input[@name='companyName'])[2]"), compName);
		//driver.findElementByClassName("companyName").sendKeys("HCL");
		
		
		click(locateElement("XPath", "//span[text()='Phone']"));
		type(locateElement("XPath", "//input[@name='phoneNumber']"), phoneNo);
		click(locateElement("XPath", "//button[text()='Find Leads']"));
		/*Thread.sleep(3000);
		System.out.println("hi");
		List<WebElement> web = driver.findElementsByXPath("//a[text()='HCL']/preceding::a[4]");
		System.out.println("hi2");
		for (WebElement webElement : web) {
			
			System.out.println("hello");
			System.out.println(webElement.getText());
		}
		*/
		
		
		
		Thread.sleep(3000);
		
		String id = locateElement("XPath", "(//a[@class='linktext'])[4]").getText();
		
		click(locateElement("LinkText", "mohammed"));
		click(locateElement("XPath", "//a[text()='Delete']"));
		click(locateElement("XPath", "//a[text()='Find Leads']"));
		type(locateElement("XPath", "//input[@name='id']"), id);
		click(locateElement("XPath", "//button[text()='Find Leads']"));
		
		Thread.sleep(3000);
		String errorMsg = locateElement("XPath", "//div[@class='x-paging-info']").getText();
		
		if (errorMsg.equals("No records to display")) {
			
			
			System.out.println("Lead with id" + " " + id + " " + "deleted successfully");

		}
		
		
		
		
		
	}







@DataProvider(name="getforDelete")
public String[][] getData() throws IOException {
	
	return ReadExcel.readData("DeleteLead");
	
	
	
	
}

















}
	


