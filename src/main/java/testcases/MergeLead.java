package testcases;


import java.io.IOException;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.SeMethods;
import week3.day1.ProjectSpecificMethods;
import week3.day1.ReadExcel;

public class MergeLead extends ProjectSpecificMethods{

	
	
	@BeforeTest
	public void setData() {
		testcaseName="TC01_MergeLead";
		testdesp="Merge Lead";
		author="Salman";
		category="Sanity";
	}
	
	@Test(dataProvider = "getforMerge")
	public void merge(String x1 , String x2 , String x3 , String name1 , String name2
			) throws InterruptedException {
		
		
	
	
	
	
	
		
			click(locateElement("LinkText", "Leads"));
			click(locateElement("LinkText", "Create Lead"));
				
		
		type((locateElement("createLeadForm_companyName")),x1);
		type((locateElement("createLeadForm_firstName")),x2);
		type((locateElement("createLeadForm_lastName")),x3);
		
		selectDropDownUsingText(locateElement("createLeadForm_dataSourceId"), "Website");
		selectDropDownUsingIndex(locateElement("createLeadForm_marketingCampaignId"),2);
		selectDropDownUsingValue(locateElement("createLeadForm_industryEnumId"), "IND_AEROSPACE");
		
		
		/*type((locateElement("createLeadForm_firstNameLocal")),"mohammed");
		type((locateElement("createLeadForm_generalProfTitle")),"TestLead");
		type((locateElement("createLeadForm_annualRevenue")),"6,00,000");
		type((locateElement("createLeadForm_ownershipEnumId")),"Corporation");
		type((locateElement("createLeadForm_sicCode")),"625002");
		type((locateElement("createLeadForm_lastNameLocal")),"fariz");*/
		
		/*type((locateElement( , "createLeadForm_departmentName")),"Testing");
		type((locateElement( , "createLeadForm_currencyUomId")),"INR - Indian Rupee");
		type((locateElement( , "createLeadForm_numberEmployees")),"200");
		type((locateElement( , "createLeadForm_tickerSymbol")),"yes");*/
		
		
		/*sendKeysUsingId("createLeadForm_departmentName","Testing");
		
		
		sendKeysUsingId("createLeadForm_currencyUomId", "INR - Indian Rupee");
		sendKeysUsingId("createLeadForm_numberEmployees","200");
		sendKeysUsingId("createLeadForm_tickerSymbol","yes");
		sendKeysUsingId("createLeadForm_description","this is the description");
		sendKeysUsingId("createLeadForm_importantNote","nothing");
		sendKeysUsingId("createLeadForm_primaryPhoneCountryCode","001");
		sendKeysUsingId("createLeadForm_primaryPhoneAreaCode","625");
		
		sendKeysUsingId("createLeadForm_primaryPhoneExtension","0452");
		sendKeysUsingId("createLeadForm_primaryPhoneNumber","9500801893");
		sendKeysUsingId("createLeadForm_primaryEmail","abc@gmail.com");
		sendKeysUsingId("createLeadForm_primaryPhoneAskForName","nobody");
		sendKeysUsingId("createLeadForm_primaryWebUrl","www.google.com");
		sendKeysUsingId("createLeadForm_generalToName","salman");
		sendKeysUsingId("createLeadForm_generalAttnName","fariz");
		sendKeysUsingId("createLeadForm_generalAddress1","22");
		sendKeysUsingId("createLeadForm_generalAddress2","road");
		sendKeysUsingId("createLeadForm_generalCity","madurai");
		sendKeysUsingId("createLeadForm_generalCountryGeoId","India");	
		*/
		/*selectDropDownUsingIndex(locateElement("XPath", "//select[@id='createLeadForm_generalStateProvinceGeoId']"), 2);
		sendKeysUsingId("createLeadForm_generalPostalCode","625002");
		sendKeysUsingId("createLeadForm_generalPostalCodeExt","002");
		*/
		click(locateElement("class", "smallSubmit"));		
		
		
		
		
		
		
		
		verifyExactAttribute(locateElement("attrValue"), "class", "inputBox");
		
		click(locateElement("LinkText", "Merge Leads"));
		click(locateElement("XPath", "//img[@alt='Lookup']"));
		switchToWindow(1);
		maximize();
		type(locateElement("XPath", "//input[@name='firstName']"), name1);
		click(locateElement("XPath", "//button[text()='Find Leads']"));
		Thread.sleep(3000);
		String text2 = getText(locateElement("XPath", "//a[@class='linktext']"));
		click(locateElement("XPath", "(//a[@class='linktext'])[3]"));
		switchToWindow(0);
		Thread.sleep(2000);
		click(locateElement("XPath", "(//img[@alt='Lookup'])[2]"));
		Thread.sleep(3000);
		switchToWindow(1);
		type(locateElement("XPath", "//input[@name='firstName']"), name2);
		click(locateElement("XPath", "//button[text()='Find Leads']"));
		maximize();
		Thread.sleep(2000);
		click(locateElement("XPath", "(//a[@class='linktext'])[3]"));
		switchToWindow(0);
		
		click(locateElement("XPath", "//a[text()='Merge']"));
		acceptAlert();
		click(locateElement("LinkText", "Find Leads"));
		type(locateElement("XPath", "//input[@name='id']"), text2);
		click(locateElement("XPath", "//button[text()='Find Leads']"));
		Thread.sleep(2000);
		String text = getText(locateElement("XPath", "//div[@class='x-paging-info']"));

		if (text.equals("No records to display")) {
			
			System.out.println("matches ma");
		}else
			
			System.out.println("no match ma");
		
		
		
		
		
		
		
		
		
		
		

	
	
	
	}

	
	
	public void sendKeysUsingId(String text1, String text2)
	{		
		type((locateElement(text1)),text2);
			
	}
	
	@DataProvider(name = "getforMerge")
	public String[][] getData() throws IOException {
		
		
		return ReadExcel.readData("MergeLead");
	}
	
	
}







