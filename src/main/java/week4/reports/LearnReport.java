package week4.reports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//path of report
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/Report.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		//attach report
		extent.attachReporter(html);
		ExtentTest test = extent.createTest("TC001_Login", "Login into leaftaps");
		test.assignAuthor("Salman");
		test.assignCategory("smoke");
		test.pass("Enter username successfully");
		test.fail("click password failed thambi");
		
		//generate report
		extent.flush();
		
		

	}

}
