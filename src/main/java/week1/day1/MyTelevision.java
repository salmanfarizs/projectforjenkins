package week1.day1;

public class MyTelevision {

	public static void main(String[] args) {
			
		Television obj1 = new Television();
		
		String channel = obj1.switchChannel(2);
		System.out.println(channel);
		
		String lang = Television.selectLang(1);
		System.out.println(lang);
		
		System.out.println(Television.tvName);
		
		String vol = obj1.switchVolume("up");
		System.out.println(vol);
		
	}

}
