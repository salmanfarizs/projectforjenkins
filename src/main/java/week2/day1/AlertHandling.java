package week2.day1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertHandling {
	
	public void alert() {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	driver.get("http://layout.jquery-dev.com/demos/iframe_local.html");
	driver.manage().window().maximize();
	/* WebElement frame = driver.findElementById("iframeResult");
	
	driver.switchTo().frame(frame);
	driver.findElementByXPath("//button[text()='Try it']").click();
	
	String text = driver.switchTo().alert().getText();
	System.out.println(text);
	driver.switchTo().alert().accept();
	
	String text2 = driver.findElementById("demo").getText();
	System.out.println(text2);
	
	driver.close();
	*/
	
	
	
	WebElement frame = driver.findElementById("childIframe");
	
	driver.switchTo().frame(frame);
	driver.findElementByXPath("(//button[text()='Close Me'])[1]").click();
	driver.findElementByXPath("(//button[text()='Close Me'])[2]").click();
	
	driver.switchTo().defaultContent();
	driver.findElementByXPath("(//button[text()='Close Me'][1])").click();
	driver.findElementByXPath("(//button[text()='Close Me'])[2]").click();

}
}