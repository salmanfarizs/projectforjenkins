package week2.day1;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public void CreateLead(String x) throws IOException {
		
	String x1 = "wipro" , x2 = "mohammed" , x3 = "salman";
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		
		
		driver.findElementById("createLeadForm_companyName").sendKeys(x);
		driver.findElementById("createLeadForm_firstName").sendKeys(x2);
		driver.findElementById("createLeadForm_lastName").sendKeys(x3);
		
		
		
		
		WebElement eleSource = driver.findElementById("createLeadForm_dataSourceId");
		WebElement eleCamp = driver.findElementById("createLeadForm_marketingCampaignId");
		WebElement eleInd = driver.findElementById("createLeadForm_industryEnumId");
		
		
		Select sc = new Select(eleSource);
		sc.selectByVisibleText("Website");
		
		Select camp = new Select(eleCamp);
		camp.selectByValue("CATRQ_AUTOMOBILE");
		
		Select ind = new Select(eleInd);
		
		//Java Collections
		List<WebElement> options = ind.getOptions();
		
		int count = options.size();
		
		System.out.println("For example");
		for (int i =0 ; i < count ; i++) {
			
			
			
			WebElement eachoption = options.get(i);
			System.out.println(eachoption.getText());
			
		}
		
		
		
		System.out.println("For each example");
		
		
		for(WebElement eachOption : options) {
			
			
			System.out.println(eachOption.getText());
			
		}
			
			
		
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("mohammed");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("TestLead");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("6,00,000");
		//driver.findElementById("createLeadForm_industryEnumId").sendKeys("Computer Software");
		driver.findElementById("createLeadForm_ownershipEnumId").sendKeys("Corporation");
		driver.findElementById("createLeadForm_sicCode").sendKeys("625002");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("fariz");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Testing");
		driver.findElementById("createLeadForm_currencyUomId").sendKeys("INR - Indian Rupee");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("200");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("yes");
		driver.findElementById("createLeadForm_description").sendKeys("this is the description");
		driver.findElementById("createLeadForm_importantNote").sendKeys("nothing");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("001");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("625");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("0452");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9500801893");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("abc@gmail.com");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("nobody");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.google.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("salman");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("fariz");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("22");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("road");
		driver.findElementById("createLeadForm_generalCity").sendKeys("madurai");
		driver.findElementById("createLeadForm_generalCountryGeoId").sendKeys("India");
		driver.findElementByXPath("//*[@id=\"createLeadForm_generalStateProvinceGeoId\"]").sendKeys("TAMILNADU");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("625002");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("002");
		driver.findElementByClassName("smallSubmit").click();
		
		//Snap
		File src = driver.getScreenshotAs(OutputType.FILE);
		 File dec = new File("./Snaps/img.jpeg");
		 FileUtils.copyFile(src, dec); 
		
		 //testcaselogic
		String Compname = driver.findElementById("viewLead_companyName_sp").getText();
		String FirstName = driver.findElementById("viewLead_firstName_sp").getText();
		String Lastname = driver.findElementById("viewLead_lastName_sp").getText();
		
		if (Compname.contains(x1))
				{
				System.out.println("Test 1 pass");
				}
		if (FirstName.equalsIgnoreCase(x2))
				System.out.println("Test 2 pass");
		
		if (Lastname.equalsIgnoreCase(x3))
			System.out.println("Test 3 pass");
		
		
		driver.close();
		 
		 
	}
	
	
	
	
	
}
