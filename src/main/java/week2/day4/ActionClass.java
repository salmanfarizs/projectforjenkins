package week2.day4;

import java.awt.Point;
import java.util.ResourceBundle.Control;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class ActionClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 RemoteWebDriver driver = new ChromeDriver() ;
		 driver.get("http://jqueryui.com/sortable/");
			driver.manage().window().maximize();
		
		Actions builder = new Actions(driver);
		
		/*driver.switchTo().frame(0);
		WebElement ele = driver.findElementByXPath("//p[text()='Drag me around']");
		
		System.out.println(ele.getLocation());
		
	
		
		
		builder.clickAndHold(driver.findElementByXPath("//p[text()='Drag me around']")).moveToElement(ele, 8, 8).perform();
		
		driver.switchTo().defaultContent();
		
		driver.findElementByLinkText("Selectable").click();
		
		driver.switchTo().frame(0);
		
		 builder.sendKeys(Keys.CONTROL)
		.click(driver.findElementByXPath("//li[text()='Item 1']"))
		.release()
		.moveToElement(driver.findElementByXPath("//li[text()='Item 2']"))
		.click()
		.release()
		.moveToElement(driver.findElementByXPath("//li[text()='Item 5']"))
		.click()
		.perform();
		*/
		 
		 
		
		
		 driver.switchTo().frame(0);
		 org.openqa.selenium.Point location = driver.findElementByXPath("//li[text()='Item 2']").getLocation();
		System.out.println(location);
		builder.dragAndDropBy(driver.findElementByXPath("//li[text()='Item 2']"), 11, 60).perform();
		 
		/* 
		 dragAndDrop(driver.findElementByXPath("//li[text()='Item 2']"), driver.findElementByXPath("//li[text()='Item 5']")).perform();
		 */
		
		
		
	}

}
